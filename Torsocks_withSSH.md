## Connexion en SSH à un onion 
Z. ABDELMOULA
24/11/2023

USE CASE : se connecter en ssh  à TJENER via  les onions tor. 

url de l'onion disponible sur https://www/~fred


1. Installation sur une machine debian ou ubuntu du paquet torsocks
`$ sudo apt install torsocks`
2. Démarrer le navigateur Tor pour déclencher la communication par *onions*.
3. Vérifier le port utilisé pour l'acheminement des paquets vis les *onions*.
![verif des logs](img/VueLogTor.png)
4. Modifier dans le fichier conf du paquet torsocks installé, le port renseigné
![Chgment du port utilisé par les onions](img/ChgConf.png)
5. Utiliser torsocks pour opérer la commande ssh 
`$ torsocks ssh <user>@<onion_url>`
![connecté](img/connected.png)

Si vous avez ce type d'erreur lors de l'utilisation de la commande précédente
![Connection refused](img/Erreur1.png)
revoir les étapes 1 à 5.

